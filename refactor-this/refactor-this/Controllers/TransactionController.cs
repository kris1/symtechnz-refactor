﻿using refactor_this.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace refactor_this.Controllers
{
    public class TransactionController : ApiController
    {
        private AccDBContext db = new AccDBContext();
        [HttpGet, Route("api/Accounts/{id}/Transactions")]
        public IHttpActionResult GetTransactions(Guid id)
        {
            if (id == null)
            {
                return BadRequest("ID was not passed");
            }
            List<Transaction> transactions = db.Transactions.Where(x => x.AccountId == id).ToList();

            if (transactions == null)
            {
                return NotFound();
            }
            return Ok(transactions);
            
        }

        [HttpPost, Route("api/Accounts/{id}/Transactions")]
        public IHttpActionResult AddTransaction(Guid id, Transaction transaction)
        {
            if (ModelState.IsValid)
            {
                db.Transactions.Add(transaction);
                db.SaveChanges();
            }

            return Ok();
            
        }
    }
}