﻿using refactor_this.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace refactor_this.Controllers
{
    public class AccountController : ApiController
    {
        private AccDBContext db = new AccDBContext();
        // GET: Account

        [HttpGet, Route("api/Accounts/{id}")]
        public IHttpActionResult GetById(Guid id)
        {
            if (id == null)
            {
                return BadRequest("ID was not passed");
            }
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                 return NotFound();
            }
            return Ok(account);
        }

        [HttpGet, Route("api/Accounts")]
        public IHttpActionResult Get()
        {

            List<Account> accounts = db.Accounts.ToList();
            return Ok(accounts);
            
        }


        [HttpPost, Route("api/Accounts")]
        public IHttpActionResult Add(Account account)
        {
            if (ModelState.IsValid)
            {
                db.Accounts.Add(account);
                db.SaveChanges();
            }

            return Ok();
        }

        [HttpPut, Route("api/Accounts/{id}")]
        public IHttpActionResult Update(Guid id, Account account)
        {
            Account existingAccount = db.Accounts.Find(id);
            existingAccount.Name = account.Name;
            db.SaveChanges();
            return Ok();
        }

        [HttpDelete, Route("api/Accounts/{id}")]
        public IHttpActionResult Delete(Guid id)
        {
            Account existingAccount = db.Accounts.Find(id);
            db.Accounts.Remove(existingAccount);
            db.SaveChanges();
            return Ok();
        }
    }
}