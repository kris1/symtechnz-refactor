﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace refactor_this.Models
{
    public class AccDBContext : DbContext
    {
        public AccDBContext()
            : base("AccDBContext")
        { }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
    }
}